using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonstruoCorrer : MonoBehaviour
{
    public GameObject monstruoCorrer;
    public float velocidadDesplazamiento = 8f;
    public float tiempoParaDesactivar = 5f;
    public GameObject detectorCorrer;
    private bool monsterAlreadyShown;

    [SerializeField]
    private AudioSource screamAudio;

    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que entro en el collider tiene el tag "Player"
        if (other.CompareTag("Player") && !monsterAlreadyShown)
        {
            monsterAlreadyShown = true;
            monstruoCorrer.SetActive(true);
            screamAudio.Play();
            Rigidbody monstruoRigidbody = monstruoCorrer.GetComponent<Rigidbody>();
            monstruoRigidbody.velocity = monstruoCorrer.transform.forward * velocidadDesplazamiento;
            Invoke("DesactivarObjeto", tiempoParaDesactivar);
        }
    }
    void DesactivarObjeto()
    {
        // Desactiva el objeto
        monstruoCorrer.SetActive(false);
    }

}
