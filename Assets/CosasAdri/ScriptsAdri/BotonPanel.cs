using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonPanel : MonoBehaviour
{
    private static event Action lightsPuzzleCompleated;

    [SerializeField]
    private AudioSource buttonPressedAudio;

    [SerializeField]
    private AudioSource SonidoFuego;

    private bool puzzleCompleated = false;

    public GameObject luz1; 
    public GameObject luz2;
    public GameObject luz3;
    public GameObject luz4;
    public GameObject luz5;
    public GameObject luz6;

    public GameObject paredFinal;

    public void Update()
    {
        if (!puzzleCompleated)
        {
            checkPuzzleCompleated();
        }
    }

    private void checkPuzzleCompleated()
    {
        if (luz1.activeSelf && luz2.activeSelf && luz3.activeSelf && 
            luz4.activeSelf && luz5.activeSelf && luz6.activeSelf) 
        {
            puzzleCompleated = true;

            lightsPuzzleCompleated?.Invoke();
            paredFinal.SetActive(false);
            SonidoFuego.Play();
        }
    }

    public void PulsarBoton1()
    {
        if (!puzzleCompleated)
        {
            buttonPressedAudio.Play();
            luz2.SetActive(true);
            luz6.SetActive(true);
        }
    }
    public void PulsarBoton2()
    {
        if (!puzzleCompleated)
        {
            buttonPressedAudio.Play();
            luz4.SetActive(true);
            luz5.SetActive(true);
            luz1.SetActive(false);
            luz3.SetActive(false);
        }
    }
    public void PulsarBoton3()
    {
        if (!puzzleCompleated)
        {
            buttonPressedAudio.Play();
            luz3.SetActive(true);
            luz6.SetActive(true);
            luz5.SetActive(false);
            luz4.SetActive(false);
        }
    }

    public void PulsarBoton4()
    {
        if (!puzzleCompleated)
        {
            buttonPressedAudio.Play();
            luz1.SetActive(true);
            luz2.SetActive(false);
            luz6.SetActive(false);
            luz3.SetActive(true);
        }
    }

}
