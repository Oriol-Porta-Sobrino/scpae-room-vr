using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements.Experimental;

public class BLUE_CUBE : MonoBehaviour
{
    public AudioSource sound;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "BLUEK")
        {
            Destroy(other.gameObject);
            sound.panStereo = -0.5f;
            sound.Play();

        }
    }
}
