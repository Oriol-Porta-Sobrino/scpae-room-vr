// Instantiates 10 copies of Prefab each 2 units apart from each other

using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Example : MonoBehaviour
{

    private Vector3 original;
    private Quaternion originalRot;
    public GameObject cubeprefab;
    public float XRotation;
    public float maxRandom, minRandom, RotationRANDOM;
    public int Duplicates;
    void Start()
    {
        original = cubeprefab.gameObject.transform.position;
        //originalRot = cubeprefab.gameObject.transform.rotation;


        for (var i = 0; i < Duplicates; i++)
        {
            float randomNumber = Random.Range(minRandom, maxRandom);
            float RotationRANDOM = Random.Range(0, 180);
            originalRot = Quaternion.Euler(RotationRANDOM, 90f, 180f); 
            
            //Instantiate(cubeprefab, new Vector3(0.0f, 0.0f, i * 2.0f) + original, Quaternion.identity);
            Instantiate(cubeprefab, new Vector3(0.0f, 0.0f, i * randomNumber * 2.0f) + original, originalRot);
            
        }
    }
}
