using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generic_Rotate_Master : MonoBehaviour
{

    public GameObject Valve1, Valve2, Valve3, Valve4;
    public GameObject Ball1, Ball2, Ball3, Ball4, Ball5, Ball6, Ball7;
    float m_Hue5, m_Hue6, m_Hue7;
    public float ExtraRotation1, ExtraRotation2, ExtraRotation3, ExtraRotation4; //Note, it has to be in degrees, from 0 to 360



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        //ToFix = Make the valves not rotate after a certain value. to avoid the player from setting the entire colors the same.

        ChangeColorValveBall(ExtraRotation1, Valve1, Ball1);
        ChangeColorValveBall(ExtraRotation2, Valve2, Ball2);
        ChangeColorValveBall(ExtraRotation3, Valve3, Ball3);
        ChangeColorValveBall(ExtraRotation4, Valve4, Ball4);

        ChangeColorBallBasedon2Balls(Ball1, Ball2, Ball5);
        ChangeColorBallBasedon2Balls(Ball3, Ball4, Ball6);
        ChangeColorBallBasedon2Balls(Ball5, Ball6, Ball7);

    }

    public static void ChangeColorValveBall(float ExtraRotation, GameObject Valve , GameObject MaterialColor)
    {
        //ToFix = ExtraRotation makes the material go Black (NULL) if it exceeds a certain value, this doens't happen if ExtraRotation is Zero.
        float m_Hue = (Valve.gameObject.transform.eulerAngles.z + ExtraRotation ) / 360;
        MaterialColor.GetComponent<Renderer>().material.color = Color.HSVToRGB(m_Hue, 1f, 1f);
    }



    public static void ChangeColorBallBasedon2Balls(GameObject MaterialColorONE,GameObject MaterialColorTWO, GameObject FinalBall) {

        Color.RGBToHSV(MaterialColorONE.GetComponent<Renderer>().material.color, out float m_HueONE, out float S1, out float V1);
        Color.RGBToHSV(MaterialColorTWO.GetComponent<Renderer>().material.color, out float m_HueTWO, out float S2, out float V2);

        float m_HueFINAL = ((m_HueONE + m_HueTWO) / 2);

        FinalBall.GetComponent<Renderer>().material.color = Color.HSVToRGB(m_HueFINAL, 1f, 1f);
        //Debug.Log("Current HSV MIX MATERIAL: " + "m_HueONE = " + m_HueONE * 360 + " || m_HueTWO = " + m_HueTWO * 360 + " || m_HueFINAL = " + m_HueFINAL);

    }



}
