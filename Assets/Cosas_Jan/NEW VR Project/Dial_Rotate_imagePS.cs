using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Dial_Rotate_imagePS : MonoBehaviour
{
    public GameObject Painting;
    public bool WantRandomizedAngles = true;
    public AudioSource Click;
    //private Quaternion rotation;
    private float x, y, Rand, Zero, Xbox;
    public float ExtraAnglesAtStart = 0;
    public bool Complete = false;
    public bool PrintDebug = false;


    // Start is called before the first frame update
    void Start()
    {
        //Gather the Painting original rotation angles.
        x = Painting.gameObject.transform.eulerAngles.x;
        y = Painting.gameObject.transform.eulerAngles.y;
        //z = Painting.gameObject.transform.eulerAngles.z;
        Rand = (Random.Range(0f, 360f));
        Zero = 0;
        Xbox = 360;

        if (WantRandomizedAngles == true)
        {
            Debug.Log("Pre Random " + Painting.transform.eulerAngles.z);
            Painting.transform.rotation = Quaternion.Euler(x, y, -180 + Rand);
            Debug.Log("Post Random " + Painting.transform.eulerAngles.z);
        }
        else {
            Painting.transform.rotation = Quaternion.Euler(x, y, -180 + ExtraAnglesAtStart);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Complete == false)
        {
            //Works but if asset was made in blender and exported to FBX.

            Painting.transform.rotation = Quaternion.Euler(x, y, -180 + Rand + this.gameObject.transform.eulerAngles.y);
            //When it reaches 0 or 360 degrees, play a Click sound and don't allow the image to move any further:

            //if ((Painting.transform.eulerAngles.z == Zero) || (Painting.transform.eulerAngles.z == Xbox))
            if ((Painting.transform.rotation == Quaternion.Euler(x, y, 0)) || (Painting.transform.rotation == Quaternion.Euler(x, y, Xbox)))
            {
                //Painting.transform.rotation = Quaternion.Euler(x, y, 0);

                if (PrintDebug == false)
                {
                    //Click.gameObject.SetActive(true);
                    Click.Play();
                    Complete = true;
                    Debug.Log(this.gameObject.name + " " + Complete);
                    PrintDebug = true;
                }
            }
        }
        else {
            Painting.transform.rotation = Quaternion.Euler(x, y, Zero);
        }

    }
}
