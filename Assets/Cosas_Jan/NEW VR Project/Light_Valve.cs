using Autohand.Demo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Light_Valve : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject LightEmitter;
    private Light lightComp;
    //public AudioSource Click;
    //private Quaternion rotation;
    //private float x, y, Rand, Zero, Xbox;
    //public bool Complete = false;
    //public bool PrintDebug = false;

    float m_Hue;
    

    void Start()
    {
        
        lightComp = LightEmitter.GetComponent<Light>();

    }

        // Update is called once per frame
        void Update()
    {
        m_Hue = (this.gameObject.transform.eulerAngles.z)/360;
        Debug.Log("Current Valve Degrees: " + m_Hue);
        lightComp.color = Color.HSVToRGB(m_Hue,1f,1f);

    }
}
