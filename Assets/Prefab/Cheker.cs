using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cheker : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> listObject = new List<GameObject>();

    [SerializeField]
    private List<GameObject> listPosition = new List<GameObject>();
    [SerializeField]
    private AudioSource padlock;
    [SerializeField]
    private GameObject candadoAbierto;
    [SerializeField]
    private GameObject candadoCerrado;

    private List<bool> listCorrect = new List<bool>();

    public static event Action checkerPuzzleCompleated;

    private bool puzzleSucceded = false;

    void Start()
    {
        prepareListCorrect();
    }

    private void Update()
    {
        if (!puzzleSucceded)
        {
            checkPositions();
            checkVictory();
        }
        
    }

    private void checkVictory()
    {
        int positionsCorrect = 0;
        foreach (bool item in listCorrect)
        {
            if (item)
            {
                positionsCorrect++;
            }
        }
        if (positionsCorrect == listCorrect.Count)
        {
            puzzleSucceded = true;
            padlock.Play();
            candadoAbierto.SetActive(true);
            candadoCerrado.SetActive(false);
            checkerPuzzleCompleated?.Invoke();
        }
    }

    private void checkPositions()
    {
        for (int i = 0; i < listPosition.Count; i++)
        {
            GameObject objectToCheck = listObject[i];
            GameObject positionToCheck = listPosition[i];
            if (Vector3.Distance(objectToCheck.transform.position, positionToCheck.transform.position) < 0.1f)
            {
                listCorrect[i] = true;
            }
            else
            {
                listCorrect[i] = false;
            }
        }
    }

    private void prepareListCorrect()
    {
        foreach (var item in listPosition)
        {
            listCorrect.Add(false);
        }
    }
}
