using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Blackboard : MonoBehaviour
{

    private Coroutine writeBlackboard;

    [SerializeField]
    private TextMeshProUGUI textMeshPro;

    [SerializeField]
    private Timer timer;

    [SerializeField]
    private AudioSource chalkWriting;

    void Start()
    {
        Globe.globePuzzleCompleated += globePuzzleCompleated;
        Cheker.checkerPuzzleCompleated += checkerMathPuzzleCompleated;
        ClockScript.checkerClockCompleated += clockPuzzleCompleated;
        ImageScript.checkerImageCompleated += imageScriptCompleated;
        writeBlackboard = StartCoroutine(writeLetterByLetter("\n\nHello again, this time I will be the one to test you\n" +
            "First thing, math class, I hope you brought your books"));
    }

    private void imageScriptCompleated()
    {
        stopWriting();
        writeBlackboard = StartCoroutine(writeLetterByLetter("Has completado el puzzle de la imagen"));
    }

    private void clockPuzzleCompleated()
    {
        stopWriting();
        writeBlackboard = StartCoroutine(writeLetterByLetter("\n\nIt's time to go out and play. Remember when leaving not to touch the light box, there could be another fire..."));
        //StartCoroutine("writeLetterByLetter", "Has completado el puzzle del reloj");
    }

    private void checkerMathPuzzleCompleated()
    {
        stopWriting();
        writeBlackboard = StartCoroutine(writeLetterByLetter("\n\nIt's time for geography class\n" +
            "Do you know where this place is? 38� N  100� O\n" +
            "Mark it with the pin that you will find in the teacher's desk drawer \n"));
        //StartCoroutine("writeLetterByLetter", "Bien hecho! \n" +
          //  "38� N  100� O");
    }

    private void globePuzzleCompleated()
    {
        stopWriting();
        writeBlackboard = StartCoroutine(writeLetterByLetter("\n\nIt's patio time. But if you want the key to get out, you'll have to wait until 03:30"));
        //StartCoroutine("writeLetterByLetter", "Molt b�, ARRIBA ESPA�A, ahora coge la llave y vete de aqui");
    }

    private void stopWriting()
    {
        StopCoroutine(writeBlackboard);
        textMeshPro.text = "";
    }

    IEnumerator writeLetterByLetter(String message)
    {
        chalkWriting.Play();
        foreach (char letter in message.ToCharArray())
        {
            yield return new WaitForSeconds(0.1f);
            textMeshPro.text += letter;
        }
        chalkWriting.Stop();
    }
}
