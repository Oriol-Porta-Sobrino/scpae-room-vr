using System;
using UnityEngine;
using UnityEngine.Events;

public class ClockScript : MonoBehaviour
{

    [SerializeField]
    private GameObject hourDial;
    [SerializeField]
    private GameObject minuteDial;
    [SerializeField]
    private AudioSource ringSound;

    public static event Action checkerClockCompleated;

    [SerializeField]
    private GameObject keyToSpawn;

    private bool puzzleCompleated, hourDialCorrect, minuteDialCorrect;

    void Update()
    {
        if (!puzzleCompleated)
        {
            checkClock();
            checkPuzzle();
        }
    }

    private void checkPuzzle()
    {
        puzzleCompleated = hourDialCorrect && minuteDialCorrect;
        if (puzzleCompleated)
        {
            keyToSpawn.SetActive(true);
            ringSound.Play();
            checkerClockCompleated?.Invoke();
        }
    }

    private void checkClock()
    {
        checkHourDial();
        checkMinuteDial();
    }

    private void checkHourDial()
    {
        if ((hourDial.transform.eulerAngles.z % 360) > 75 && (hourDial.transform.eulerAngles.z % 360) < 105)
        {
            hourDialCorrect = true;
        } else
        {
            hourDialCorrect = false;
        }
    }

    private void checkMinuteDial()
    {
        if ((minuteDial.transform.eulerAngles.z % 360) > 165 && (minuteDial.transform.eulerAngles.z % 360) < 195)
        {
            minuteDialCorrect = true;
        } else
        {
            minuteDialCorrect = false;
        }
    }
}
