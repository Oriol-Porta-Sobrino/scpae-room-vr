using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorrectCountryGlobe : MonoBehaviour
{

    public bool correctLocationStabbed = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 29) 
        {
            correctLocationStabbed = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 29)
        {
            correctLocationStabbed = false;
        }
    }

}
