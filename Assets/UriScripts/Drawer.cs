using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawer : MonoBehaviour
{

    [SerializeField]
    private ConfigurableJoint configurableJoint;

    void Start()
    {
        Cheker.checkerPuzzleCompleated += unlockDrawer;
        lockDrawer();
    }

    void Update()
    {
        
    }

    private void unlockDrawer()
    {
        configurableJoint.xMotion = ConfigurableJointMotion.Limited;
    }

    private void lockDrawer()
    {
        configurableJoint.xMotion = ConfigurableJointMotion.Locked;
    }
}
