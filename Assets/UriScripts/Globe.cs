using Autohand;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Globe : MonoBehaviour
{

    private List<Stabber> stabberList = new List<Stabber>();
    private CorrectCountryGlobe correctStab;

    public static event Action globePuzzleCompleated;

    [SerializeField]
    private List<GameObject> stabbers;
    [SerializeField]
    private AudioSource shutDownSound;

    [SerializeField]
    private GameObject torch;
    private bool puzzleCompleated = false;

    void Start()
    {
        correctStab = GetComponentInChildren<CorrectCountryGlobe>();
    }

    void Update()
    {
        shouldGlobaRotateBeGrabbable();
        if (!puzzleCompleated)
        {
            checkCorrectCountry();
        }
    }

    private void shouldGlobaRotateBeGrabbable()
    {
        stabberList = gameObject.GetComponent<Stabbable>().getStabbingList();
        if (stabberList.Any())
        {
           gameObject.GetComponent<Grabbable>().enabled = false;
        } else
        {
            gameObject.GetComponent<Grabbable>().enabled = true;
        }
    }

    public void checkCorrectCountry()
    {
        stabberList = gameObject.GetComponent<Stabbable>().getStabbingList();
        if (stabberList.Any())
        {
            if (stabbers.Contains(stabberList[0].gameObject))
            {
                if (correctStab.correctLocationStabbed)
                {
                    puzzleCompleated = true;
                    shutDownSound.Play();
                    torch.SetActive(true);
                    globePuzzleCompleated?.Invoke();
                }
            }
        }
    }

}
