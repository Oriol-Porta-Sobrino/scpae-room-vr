using System;
using UnityEngine;

public class ImageScript : MonoBehaviour
{
    [SerializeField]
    private GameObject centerImagePart;
    [SerializeField]
    private GameObject middleImagePart;
    [SerializeField]
    private GameObject outerImagePart;
    [SerializeField]
    private GameObject candadoCerrado;

    [SerializeField]
    private GameObject candadoAbierto;

    public static event Action checkerImageCompleated;

    private bool imagePuzzleCompleated, centerImageCorrect, middleImageCorrect, outerImageCorrect;
    private Vector3 originalCenterImagePosition;
    private Vector3 originalMiddleImagePosition;
    private Vector3 originalOuterImagePosition;

    void Start()
    {
        originalCenterImagePosition = centerImagePart.transform.eulerAngles;
        originalMiddleImagePosition = middleImagePart.transform.eulerAngles;
        originalOuterImagePosition = outerImagePart.transform.eulerAngles;
        centerImagePart.transform.eulerAngles = new Vector3(centerImagePart.transform.eulerAngles.x, centerImagePart.transform.eulerAngles.y, -180 + UnityEngine.Random.Range(0f, 360f));
        middleImagePart.transform.eulerAngles = new Vector3(middleImagePart.transform.eulerAngles.x, middleImagePart.transform.eulerAngles.y, -180 + UnityEngine.Random.Range(0f, 360f));
        outerImagePart.transform.eulerAngles = new Vector3(outerImagePart.transform.eulerAngles.x, outerImagePart.transform.eulerAngles.y, -180 + UnityEngine.Random.Range(0f, 360f));
    }

    void Update()
    {
        if (!imagePuzzleCompleated)
        {
            checkPuzzle();
            checkPuzzleCompleated();
        }
    }

    private void checkPuzzleCompleated()
    {
        imagePuzzleCompleated = centerImageCorrect && middleImageCorrect && outerImageCorrect;
        if (imagePuzzleCompleated)
        {
            candadoCerrado.SetActive(false);
            candadoAbierto.SetActive(true);
            checkerImageCompleated?.Invoke();
        }
    }

    private void checkPuzzle()
    {
        checkImagePart(centerImagePart, originalCenterImagePosition, ref centerImageCorrect);
        checkImagePart(middleImagePart, originalMiddleImagePosition, ref middleImageCorrect);
        checkImagePart(outerImagePart, originalOuterImagePosition, ref outerImageCorrect);
    }

    private void checkImagePart(GameObject imageToCheck, Vector3 correctImagePosition, ref bool correctPosition)
    {
        if ((imageToCheck.transform.eulerAngles.z % 360) > correctImagePosition.z -20 && 
            (imageToCheck.transform.eulerAngles.z % 360) < correctImagePosition.z + 20)
        {
            correctPosition = true;
        } else
        {
            correctPosition = false;
        }
    }
}
