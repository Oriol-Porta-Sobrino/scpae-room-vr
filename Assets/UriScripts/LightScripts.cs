using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightScripts : MonoBehaviour
{

    [SerializeField]
    private bool luzparpadea = false;
    private bool titilando = false;
    private bool lightShutDown = false;
    private float timeDelay;

    private void Start()
    {
        //Puzzle.puzzleCompleated += encenderLuz;
        Globe.globePuzzleCompleated += shutDown;
    }

    void Update()
    {
        if (luzparpadea == true && titilando == false)
        {
            StartCoroutine(Titilar());
        }
    }

    private void shutDown()
    {
        lightShutDown = true;
        this.gameObject.GetComponent<Light>().enabled = false;
    }

    private void encenderLuz()
    {
        if (!lightShutDown)
        {
            this.gameObject.GetComponent<Light>().enabled = true;
        }
    }

    private void apagarLuz()
    {
        this.gameObject.GetComponent<Light>().enabled = false;
    }

    IEnumerator Titilar()
    {
        titilando = true;
        float tiempoEntreTitileo = Random.Range(10.0f, 20.0f);
        yield return new WaitForSeconds(tiempoEntreTitileo);
        int tiempoTitilando = Random.Range(3, 8);
        for (int i = 0; i < tiempoTitilando; i++)
        {
            yield return StartCoroutine(LuzQueTitila());
        }
        titilando = false;
    }

    IEnumerator LuzQueTitila()
    {
        apagarLuz();
        timeDelay = Random.Range(0.01f, 0.2f);
        yield return new WaitForSeconds(timeDelay);
        encenderLuz();
        timeDelay = Random.Range(0.01f, 0.2f);
        yield return new WaitForSeconds(timeDelay);
    }
}
