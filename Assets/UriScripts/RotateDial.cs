using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateDial : MonoBehaviour
{
    [SerializeField]
    private GameObject rotatingObject;

    [SerializeField]
    private AudioSource tikTakSound;

    private float x, y;

    // Start is called before the first frame update
    void Start()
    {
        Globe.globePuzzleCompleated += unlockDials;
        ClockScript.checkerClockCompleated += lockDials;
        //Gather the Painting original rotation angles.
        x = rotatingObject.gameObject.transform.eulerAngles.x;
        y = rotatingObject.gameObject.transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        //Works but if asset was made in blender and exported to FBX.
        rotatingObject.transform.eulerAngles = new Vector3(x, y, this.gameObject.transform.eulerAngles.y - 90);
        //When it reaches 0 or 360 degrees, play a Click sound and don't allow the image to move any further:
    }

    private void unlockDials()
    {
        if (tikTakSound != null)
        {
            tikTakSound.Play();
        }
        GetComponent<HingeJoint>().useLimits = false;
    }

    private void lockDials()
    {
        if (tikTakSound != null)
        {
            tikTakSound.Stop();
        }
        GetComponent<Rigidbody>().isKinematic = true;
    }
}
