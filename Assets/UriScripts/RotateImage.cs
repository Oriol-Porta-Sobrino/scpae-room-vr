using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateImage : MonoBehaviour
{
    [SerializeField]
    private GameObject rotatingObject;

    private float x, y;

    // Start is called before the first frame update
    void Start()
    {
        ImageScript.checkerImageCompleated += lockDials;
        Cheker.checkerPuzzleCompleated += unlockDials;
        //Gather the Painting original rotation angles.
        x = rotatingObject.gameObject.transform.eulerAngles.x;
        y = rotatingObject.gameObject.transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.transform.eulerAngles.y != 0)
        {
            if (this.gameObject.transform.eulerAngles.y < 1)
            {
                Debug.Log("Derecha " + (this.gameObject.transform.eulerAngles.y));
                rotatingObject.transform.rotation = Quaternion.Euler(x, y, rotatingObject.transform.eulerAngles.z + 0.5f);
            }
            else if (this.gameObject.transform.eulerAngles.y > 1)
            {
                Debug.Log("Izquierda " + (this.gameObject.transform.eulerAngles.y));
                rotatingObject.transform.rotation = Quaternion.Euler(x, y, rotatingObject.transform.eulerAngles.z - 0.5f);
            }
        }
        
    }

    private void unlockDials()
    {
        GetComponent<HingeJoint>().useLimits = false;
    }

    private void lockDials()
    {
        GetComponent<HingeJoint>().useLimits = true;
    }
}
