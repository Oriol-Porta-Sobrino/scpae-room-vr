using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrorSounds : MonoBehaviour
{

    [SerializeField]
    private AudioSource terrorSound1;
    [SerializeField]
    private AudioSource terrorSound2;
    private bool readyToSound1;
    private bool readyToSound2;

    // Update is called once per frame
    void Update()
    {
        if (!readyToSound1)
        {
            StartCoroutine(SonidoTerror1());
        }
        if (!readyToSound2)
        {
            StartCoroutine(SonidoTerror2());
        }
    }

    IEnumerator SonidoTerror1()
    {
        readyToSound1 = true;
        float tiempoEntreTitileo = Random.Range(60.0f, 120.0f);
        yield return new WaitForSeconds(tiempoEntreTitileo);
        terrorSound1.Play();
        readyToSound1 = false;
    }

    IEnumerator SonidoTerror2()
    {
        readyToSound2 = true;
        float tiempoEntreTitileo = Random.Range(60.0f, 120.0f);
        yield return new WaitForSeconds(tiempoEntreTitileo);
        terrorSound2.Play();
        readyToSound2 = false;
    }
}
